import java.util.*;

public class Validator
{
	String lex;  // contains current token

	public void validate(List<String> toks)  // exits program if expression is invalid
	{
		if (!B(toks.iterator())) // if not a valid boolean expression
		{
			System.err.println("INVALID: Expression not valid");
			System.exit(-1); // exit program so we dont interpret a wrong expression
		}
	}

	void getToken(Iterator<String> it)
	{
		try
		{
			lex = it.next();  // try to get next token
		}
		catch (NoSuchElementException e)  // if no next token
		{
			System.err.println("INVALID: Incomplete expression");
			System.exit(-1);  // exit program
		}
	}

	boolean B(Iterator<String> it)
	{
		getToken(it);
		if (IT(it))
		{
			if (lex.equals(".") && !it.hasNext())
				return true;
			return false;
		}
		return false;
	}

	boolean IT(Iterator<String> it)
	{
		if (OT(it))
		{
			if (IT_Tail(it))
				return true;
			return false;
		}
		return false;
	}

	boolean IT_Tail(Iterator<String> it)
	{
		if (lex.equals("->"))
		{
			getToken(it);
			if (OT(it))
			{
				if (IT_Tail(it))
					return true;
				return false;
			}
			return false;
		}
		return true;
	}

	boolean OT(Iterator<String> it)
	{
		if (AT(it))
		{
			if (OT_Tail(it))
				return true;
			return false;
		}
		return false;
	}

	boolean OT_Tail(Iterator<String> it)
	{
		if (lex.equals("v"))
		{
			getToken(it);
			if (AT(it))
			{
				if (OT_Tail(it))
					return true;
				return false;
			}
			return false;
		}
		return true;
	}

	boolean AT(Iterator<String> it)
	{
		if (L(it))
		{
			if (AT_Tail(it))
				return true;
			return false;
		}
		return false;
	}

	boolean AT_Tail(Iterator<String> it)
	{
		if (lex.equals("^"))
		{
			getToken(it);
			if (L(it))
			{
				if (AT_Tail(it))
					return true;
				return false;
			}
			return false;
		}
		return true;
	}

	boolean L(Iterator<String> it)
	{
		if (lex.equals("~"))
		{
			getToken(it);
			if (L(it))
				return true;
			return false;
		}
		else if (A(it))
			return true;
		return false;
	}

	boolean A(Iterator<String> it)
	{
		if (lex.equals("T") || lex.equals("F"))
		{
			getToken(it);
			return true;
		}
		else if (lex.equals("("))
		{
			getToken(it);
			if (IT(it))
			{
				if (lex.equals(")"))
				{
					getToken(it);
					return true;
				}
				return false;
			}
			return false;
		}
		return false;
	}
}
