import java.util.*;

public class Project
{
	public static void main(String[] args)
	{
		Scanner bab = new Scanner(System.in);
		System.out.print("Enter Expression: ");
		String input = bab.nextLine().trim();
		
		Interpreter interpreter = new Interpreter(input);
		interpreter.evaluate();
	}
}
