import java.util.*;

public class Interpreter
{
	List<String> tokens; // list of tokens delimmited by a space

	public Interpreter(String expr)
	{
		tokens = new ArrayList<String>(Arrays.asList(expr.split(" ")));
	}

	public void evaluate()
	{
		Validator val = new Validator();
		val.validate(tokens);  // if program gets past here, expression is valid
		tokens = tokens.subList(0, tokens.size() - 1);  // removes . at end
		// removes negations and transforms it into postfix notation
		tokens = intoPostfix(removeNegations(tokens));

		Stack<Boolean> stack = new Stack<Boolean>();  // result stack
		boolean term1, term2;  // holders for the popped values
		for (String temp : tokens)  // loop through the tokens list 
		{
			if (temp.equals("T"))   // if encountering value, push to stack
				stack.push(true);
			else if (temp.equals("F"))
				stack.push(false);
			else  // encountered an operator
			{
				term2 = stack.pop();  //populate the boolean variables
				term1 = stack.pop();
				switch (temp)   // switch on the operator
				{
					case "v":
						stack.push(term1 || term2);
						break;
					case "^":
						stack.push(term1 && term2);
						break;
					case "->":
						stack.push(!term1 || term2); // implies translates to !P || Q
						break;
					case "x>":
						stack.push(term1 && !term2); // doesnt imply translates to P && !Q
						break;
				}
			}
		}
		System.out.println("Result: " + stack.pop()); // result is the only thing in the stack
	}

	// computes negations ahead of time as to make the final expression
	// void of unary operators
	List<String> removeNegations(List<String> list)
	{
		int negIndex;   // index of ~
		while (list.contains("~"))
		{
			negIndex = list.lastIndexOf("~");
			list.remove(negIndex);             // remove ~ character, shifts remaining tokens right
			String temp = list.get(negIndex);  // get new token that slid into the negation spot
			if (temp.equals("("))              // if start of group
			{
				int parenCount = 1;            // keep track of parenthesis
				int i = negIndex + 1;          // set current index to token after (
				while (parenCount != 0)        // parenCount == 0 when found match to first paren
				{
					temp = list.get(i);        // store token at position in temp
					switch (temp)              // negate whatever it is and replace
					{
						case "T":
							list.set(i, "F");
							break;
						case "F":
							list.set(i, "T");
							break;
						case "^":
							list.set(i, "v");
							break;
						case "v":
							list.set(i, "^");
							break;
						case "->":
							list.set(i, "x>");  // x> is a custom temp operator equal to !implies
							break;
						case "x>":
							list.set(i, "->");
							break;
						case ")":
							parenCount--;
							break;
						case "(":
							parenCount++;
							break;
					}
					i++;  // increment place
				}
			}
			else // negate constant only
			{
				switch (temp)
				{
					case "T":
						list.set(negIndex, "F");
						break;
					case "F":
						list.set(negIndex, "T");
						break;
				}
			}
		}
		return list;
	}

	// returns the precedence of each operator
	int precedenceOf(String tok)
	{
		switch (tok)
		{
			// doesnt need negation because they were alreay removed from expression
			case "^":  return 2;
			case "v":  return 1;
			case "->":
			case "x>": return 0;  // x> is a temp custom token for !implies	
		}
		return -1;
	}

	// This uses the Shunting-Yard algorithm to translate
	// infix to postfix notation of the expression
	// this is a well known algorithm
	List<String> intoPostfix(List<String> list)
	{
		// Wont have to deal with negation because they will 
		// be removed by removeNegations()
		Stack<String> stack = new Stack<String>();
		List<String> postfix = new ArrayList<String>();

		String element;
		for (int i = 0; i < list.size(); i++)
		{
			element = list.get(i);
			if (element.equals("T") || element.equals("F"))
				postfix.add(element);
			else if (element.equals("("))
				stack.push(element);
			else if (element.equals(")"))
			{
				while (!stack.empty() && !stack.peek().equals("("))
					postfix.add(stack.pop());
				stack.pop();
			}
			else
			{
				if (stack.empty() || stack.peek().equals("("))
					stack.push(element);
				else
				{
					while (!stack.empty() && (precedenceOf(element) <= precedenceOf(stack.peek())))
						postfix.add(stack.pop());
					stack.push(element);
				}
			}
		}
		while (!stack.empty())
			postfix.add(stack.pop());
		return postfix;
	}
}
